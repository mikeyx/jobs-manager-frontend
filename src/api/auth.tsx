import axios from "axios";
axios.defaults.withCredentials = true;

export const onRegistration = async (registrationData: any) => {
  return await axios.post(
    "https://jobio-app-api.onrender.com/api/register",
    registrationData
  );
};

export const onLogin = async (loginData: any) => {
  return await axios.post(
    "https://jobio-app-api.onrender.com/api/login",
    loginData
  );
};

export const onLogout = async () => {
  return await axios.get("https://jobio-app-api.onrender.com/api/logout");
};
